# Vanilla JS Skeleton

This is an opinionated starter skeleton for coding vanilla es6+ JavaScript.

Currently it only includes two tools that are recommended as global installs, but more local installs are to come. This will likely be published to npm down the road for easier consumption and use

## Upcoming additions!

* **Transpiling**: Notably absent in the current version meaning that advanced JS features not yet implemented in either node or browser engines might not work
* **Unit Tests**: Basic testing library to get up and running really quickly
* **Basic Test Files**: Create some basic sample files as examples

## Required Installs

1.  Ensure that you have node and npm installed
2.  Global Installs (to run against all relevant projects)
    * (prettier) `npm i -g prettier`
    * (eslint) `npm i -g eslint`

## Editor Usage

### VS Code

1.  Install both the prettier and eslint extensions
2.  Configure the editor settings
    * `"editor.formatOnSave": true`,
    * `"eslint.enable": true,`
